####Recommended requirements

 - Docker - latest version
 - Docker-compose - latest version


####Setup

 - clone repository & enter to the project dir
 - enter command `docker-compose up -d`in terminal
 - enter to mysql container typing `docker-compose exec mysql bash`
 - enter to mysql typing `mysql -uroot -pmysql`
 - create database typing `create database wskz;`
 - logout from the mysql container and type `docker-compose exec -T mysql mysql -uroot -pmysql wskz < app/dump.sql`
 
 
####Run
 - Go to `http://localhost:80` to see an application