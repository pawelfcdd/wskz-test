<?php
session_start();

require_once '../vendor/autoload.php';

$app = new \App\Controller\AppController();

$app->run();