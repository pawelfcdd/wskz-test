<?php


namespace App\Controller;


use App\Model\UserModel;

class AuthController
{
    private UserModel $database;

    public function __construct(UserModel $database)
    {
        $this->database = $database;
    }

    public function authUser(array $data)
    {
        $username = trim($data['username']);
        $password = trim($data['password']);
        $user = $this->database->getUser($username, $password);

        if ($user) {
            if (!isset($_SESSION['user_logged'])) {
                $_SESSION['user_login'] = $username;
                $_SESSION['user_logged'] = true;
            }
            $this->database->saveLoginAction($user);
            return true;
        }

        return false;
    }
}
