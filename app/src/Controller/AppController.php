<?php

namespace App\Controller;

use App\Model\UserModel;
use App\View\ViewController;

class AppController
{
    private UserModel $db;
    private ViewController $view;

    public function __construct()
    {
        $this->db = new UserModel();
        $this->view = new ViewController();
    }

    /**
     * @throws \Exception
     */
    public function run()
    {
        $content = null;
        $requestUri = $_SERVER['REQUEST_URI'];
        $auth = new AuthController($this->db);

        switch ($requestUri) {
            case '/login':
                if ($_SERVER['REQUEST_METHOD'] === 'GET') {
                    if (isset($_SESSION['user_login']) && $_SESSION['user_login']) {
                        header('location: /profile');
                    } else {
                        $content = $this->view->loginForm();
                    }
                }

                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    if ($auth->authUser($_POST)) {
                        header('location: /profile');
                    } else {
                        $_SESSION['alert_message'] = 'Bad credentials';
                        header('location: /login');
                    }
                }
                break;
            case '/logout':
                $this->logout();
                break;
            case '/profile':
                if (isset($_SESSION['user_login']) && $_SESSION['user_login']) {
                    $user = $this->db->getUser($_SESSION['user_login']);
                    $content = $this->view->profilePage($user);
                } else {
                    header('location: /login');
                }
                break;
            case '/register':
                if ($_SERVER['REQUEST_METHOD'] === 'GET') {
                    $content = $this->view->registerForm();
                }

                if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                    if (!$this->db->createUser($_POST)) {
                        header('location: /register');
                    } else {
                        header('location: /login');
                    }
                }
                break;
            default:
                if (isset($_SESSION['user_login']) && $_SESSION['user_login']) {
                    header('location: /profile');
                } else {
                    $content = '';
                }
                break;
        }

         return $this->view->renderDocument($content);
    }

    private function logout()
    {
        $_SESSION = array();
        session_destroy();
        session_start();
        $_SESSION['success_message'] = 'You are successfully logged out';
        header('location: /login');
        exit;
    }
}