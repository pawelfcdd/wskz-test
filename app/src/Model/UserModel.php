<?php


namespace App\Model;


class UserModel
{
    private \PDO $connection;
    private string $host = 'mysql';
    private string $user = 'root';
    private string $password = 'mysql';
    private string $dbName = 'wskz';

    public function __construct()
    {
        $connectionString = 'mysql:host=%s;dbname=%s';
        try {
            $this->connection = new \PDO(
                sprintf($connectionString, $this->host, $this->dbName),
                $this->user,
                $this->password
            );
            $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $exception) {
            echo 'Connection failed: ' . $exception->getMessage();
        }
    }

    /**
     * @param array $user
     * @return $this
     */
    public function saveLoginAction(array $user)
    {
        $date = new \DateTime();
        $query = 'INSERT INTO login_history VALUES (NULL, ?, ?, ?)';
        $stmt = $this->connection->prepare($query);
        $stmt->execute([
            $user['id'],
            $date->getTimestamp(),
            $_SERVER['REMOTE_ADDR'],
        ]);

        return $this;
    }

    /**
     * @param array $userData
     * @return bool
     */
    public function createUser(array $userData)
    {
        if (strlen($userData['username']) < 6) {
            $_SESSION['alert_message'] = 'Username should contains 6 or more digits!';
            return false;
        }

        if (strlen($userData['password']) < 6) {
            $_SESSION['alert_message'] = 'Password should contains 8 or more digits!';
            return false;
        }

        if (!$this->ifUserExist($userData['username'])) {
            $query = 'INSERT INTO users VALUES (NULL, ?, ?, ?, ?, ?)';
            $stmt = $this->connection->prepare($query);
            $stmt->execute([
                $userData['username'],
                hash('sha256', $userData['password']),
                $userData['first_name'],
                $userData['last_name'],
                $userData['sex'],
            ]);

        } else {
            $_SESSION['alert_message'] = 'User already exists!';
            return false;
        }

        $_SESSION['success_message'] = 'User was successfully created! Now You can log in';

        return true;
    }

    /**
     * @param string $username
     * @param string $password
     * @return mixed
     */
    public function getUser(string $username, string $password = null)
    {
        $query = 'SELECT * FROM users WHERE username=?';

        if ($password) {
            $query .= ' AND password=?';
        }


        $stmt = $this->connection->prepare($query);
        $parameters = [
            $username
        ];

        if ($password) {
            array_push($parameters, hash('sha256', $password));
        }

        $stmt->execute($parameters);

        return $stmt->fetch();
    }

    /**
     * @param $username
     * @return mixed
     */
    private function ifUserExist($username)
    {
        $query = 'SELECT * FROM users WHERE username = ?';
        $stmt = $this->connection->prepare($query);
        $stmt->execute([$username]);

        return $stmt->fetch();
    }

}