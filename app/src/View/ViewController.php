<?php

namespace App\View;

class ViewController
{
    /**
     * @param string|null $content
     */
    public function renderDocument(string $content = null)
    {
        $document = sprintf($this->createDocument(), $content);
        $domDoc = new \DOMDocument();
        $domDoc->loadHTML($document);

        echo $domDoc->saveHTML();
    }

    public function profilePage($user)
    {
        return '<div class="row"><div class="col-12">
                        <p>Good day, '.$user['first_name'] . ' ' . $user['last_name'].'</p>
                        <p>Your gender is '.$user['sex'] .'</p>
                        <p>Your last login was  '.$user['sex'] .'</p>
                        <p><a class="btn-warning btn" href="/logout">Logout</a></p>
                </div></div>';
    }
    /**
     * @return string
     */
    public function loginForm()
    {
        $message = null;

        if (isset($_SESSION['success_message'])) {
            $message = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'.$_SESSION['success_message'].'</p></div>';
            unset($_SESSION['success_message']);
        }

        if (isset($_SESSION['alert_message'])) {
            $message = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'.$_SESSION['alert_message'].'</p></div>';
            unset($_SESSION['alert_message']);
        }

        return $message . '<form name="login" method="post">
                     <div class="form-group">
                        <label for="username">username</label>
                        <input type="text" class="form-control" name="username" id="username" min="6" required/>    
                    </div>
                    <div class="form-group">
                        <label for="password">password</label>
                         <input type="password" class="form-control" name="password" id="password" required/>
                    </div>
                    <button type="submit" class="btn btn-info">Login</button> 
                    <a href="/register" class="btn btn-success">Register</a>
                 </form>';
    }
    /**
     * @return string
     */
    public function registerForm()
    {
        $message = null;

        if (isset($_SESSION['alert_message'])) {
            $message = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><p>'.$_SESSION['alert_message'].'</p></div>';
            unset($_SESSION['alert_message']);
        }

        return $message . '<form name="register" method="post">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" name="username" id="username" min="6" required/>    
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                         <input type="password" class="form-control" name="password" id="password" required/>
                    </div>
                    <div class="form-group">
                        <label for="first_name">First name</label>
                    <input type="text"  class="form-control" name="first_name" id="first_name" required> 
                    </div>
                    <div class="form-group">
                        <label for="last_name">Last name</label>
                        <input type="text"  class="form-control" name="last_name" id="last_name" required> 
                    </div>
                     <div class="form-group">
                        <label for="sex">Gender</label>
                        <select name="sex" class="form-control" id="sex" required>
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-success">Register</button> 
                    <a href="/login" class="btn btn-info">Login</a>
                 </form>';
    }

    private function createDocument()
    {
        $htmlOpen = '<html>';
        $head = '<head>
                    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css" type="text/css">
                    <script src="/assets/jquery/jquery-3.5.1.min.js"></script>
                    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
                 </head>';
        $body = '<body><div class="container">%s</div></body>';
        $htmlClose = '</html>';

        return $htmlOpen . $head . $body . $htmlClose;
    }
}