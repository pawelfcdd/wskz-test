create table users
(
    id int auto_increment,
    username varchar(255) not null,
    password varchar(255) not null,
    first_name varchar(255) not null,
    last_name varchar(255) not null,
    sex varchar(5) not null,
    constraint user_pk
        primary key (id)
);

create unique index users_username_uindex
	on users (username);

create table login_history
(
	id int auto_increment,
	user_id int not null,
	logged_at varchar(255) not null,
	user_ip varchar(255) not null,
	constraint login_history_pk
		primary key (id)
);
